<h1 align="center">Hi 👋, I'm sanjo Varghese</h1>
<h3 align="center">A passionate IT proffesional</h3>

- 🌱 I’m currently learning **Python**

- 💬 Ask me about **Cloud,Cyber security,System**

- 📫 How to reach me **sanjovarghese484@gmail.com**

- ⚡ Fun fact **Iam a Noob**

<h3 align="left">Connect with me:</h3>
<p align="left">
<a href="https://linkedin.com/in/sanjo-varghese-18a016269" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="sanjo-varghese-18a016269" height="30" width="40" /></a>
<a href="https://fb.com/sanjovarghese.net" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/facebook.svg" alt="sanjovarghese.net" height="30" width="40" /></a>
<a href="https://instagram.com/sanjo._.varghese" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/instagram.svg" alt="sanjo._.varghese" height="30" width="40" /></a>
</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://www.arduino.cc/" target="_blank" rel="noreferrer"> <img src="https://cdn.worldvectorlogo.com/logos/arduino-1.svg" alt="arduino" width="40" height="40"/> </a> <a href="https://aws.amazon.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/amazonwebservices/amazonwebservices-original-wordmark.svg" alt="aws" width="40" height="40"/> </a> <a href="https://azure.microsoft.com/en-in/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/microsoft_azure/microsoft_azure-icon.svg" alt="azure" width="40" height="40"/> </a> <a href="https://www.cprogramming.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg" alt="c" width="40" height="40"/> </a> <a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a> <a href="https://www.docker.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/docker/docker-original-wordmark.svg" alt="docker" width="40" height="40"/> </a> <a href="https://dotnet.microsoft.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/dot-net/dot-net-original-wordmark.svg" alt="dotnet" width="40" height="40"/> </a> <a href="https://cloud.google.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/google_cloud/google_cloud-icon.svg" alt="gcp" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://grafana.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/grafana/grafana-icon.svg" alt="grafana" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://www.adobe.com/in/products/illustrator.html" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/adobe_illustrator/adobe_illustrator-icon.svg" alt="illustrator" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://www.jenkins.io" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/jenkins/jenkins-icon.svg" alt="jenkins" width="40" height="40"/> </a> <a href="https://kubernetes.io" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/kubernetes/kubernetes-icon.svg" alt="kubernetes" width="40" height="40"/> </a> <a href="https://www.linux.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> <a href="https://www.mongodb.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mongodb/mongodb-original-wordmark.svg" alt="mongodb" width="40" height="40"/> </a> <a href="https://www.photoshop.com/en" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg" alt="photoshop" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> <a href="https://reactjs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> </a> <a href="https://www.vagrantup.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/vagrantup/vagrantup-icon.svg" alt="vagrant" width="40" height="40"/> </a> </p>


# Build and Deploy a Modern YouTube Clone Application in React JS with Material UI 5
 # Architecture
![gitlab](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/6ed0ac05-5979-4613-bc61-8e3c317c9bb1)

![YouTube](https://i.ibb.co/4R5RkmW/Thumbnail-5.png)

### Showcase your dev skills with practical experience and land the coding career of your dreams
💻 JS Mastery Pro - https://jsmastery.pro/youtube
✅ A special YOUTUBE discount code is automatically applied!

📙 Get the Ultimate Frontend & Backend Development Roadmaps, a Complete JavaScript Cheatsheet, Portfolio Tips, and more - https://www.jsmastery.pro/links

# Procedure
- Step 1: Create an API key for Youtube.
- Step 2: Create a Repository and push it to GitLab.
- Step 3: Launch an Ec2 instance and run Sonarqube on it.
- Step 4A: Create a .gitlab-ci.yml File.
- Step 4B: Add the required variables for the project.
- Step 5: Install Gitlab Runner on Ec2.
- Step 6: Run the Application on the Docker container.
- Step 7: Access the Application on Browser.
# Step 1: Create an API key from Rapid API
- Open a new tab in the browser and search for rapidapi.com
- It will automatically provide your mail and select a mail to create an account

![rapidapi](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/540b11ae-e639-4635-86f1-5f0046f9f952)

**Account is created**
- Now in the search bar search for YouTube and select YouTube v3

![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/8bcbc23f-b02a-4d42-b255-5c2398e5c3c1)
**Copy API and save it for further use at the docker stage.**
- docker build --build-arg REACT_APP_RAPID_API_KEY=<API-KEY> -t ${imageName} .

![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/1a0afc79-7383-48ed-a57f-443f1490b7d6)


# Step 2: Create a Repository and push it to GitLab
Go to GitLab.com and login to your account
- Click on New Project
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/bfb40a50-a592-4872-9a13-a2e54e3330fd)
Click on Create Blank Project
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/dfe9b4c5-e707-4530-8eee-3a8f332ad60e)

- Provide a name for the Project
- Keep Visibility to the public
Uncheck the Readme and create the Project.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/7fb4dfeb-a862-4b3a-bcc2-5dded7f07628)

Use the below commands to push code to GitLab
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/1e084232-dd4c-44c9-aaf7-2298c955f4f8)
Files pushed to GitLab.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/1bc545b1-26ee-4080-b9b7-50ac6665e0b1)

# Step 3: Launch an Ec2 instance and run Sonarqube on it

**Log into AWS Console**: Sign in to your AWS account.
**Launch an Instance:**
Choose **"EC2"** from services. Click "Launch Instance."
**Choose an AMI:** Select an Ubuntu image.
Choose an Instance Type: Pick **"t2.medium."**
**Key Pair:** Choose an existing key pair or create a new one.
**Configure Security Group:**
Create a new security group. Add rules for **HTTP, and HTTPS,** and open all ports for learning purposes. Add Storage: Allocate at least 10 GB of storage.
**Launch Instance:** Review and launch the instance.
Access Your Instance: Use SSH to connect to your instance with the private key.
Keep in mind, that opening all ports is not recommended for production environments; it's just for educational purposes.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/7420ed7f-4d4c-47a6-8b49-2504578aa0c6)

# Connect to Your EC2 Instance and install docker:
Run the below commands to install the docker
**COPY Commands:**
```sh 
sudo apt-get update
sudo apt-get install docker.io -y
sudo usermod -aG docker $USER   #my case is ubuntu
newgrp docker
sudo chmod 777 /var/run/docker.sock
```
After the docker installation, we will create a Sonarqube container (Remember to add 9000 ports in the security group).
Run this command on your EC2 instance to create a SonarQube container:
**copy**
```sh 
docker run -d --name sonar -p 9000:9000 sonarqube:lts-community
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/bb61b196-493d-4e52-853c-bbaf1bc4b9a9)
Now copy the IP address of the ec2 instance
```sh
<ec2-public-ip:9000>
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/75882e04-7664-401a-a82d-1a6415fbed09)
Enter username and password, click on login and change password

```sh
username admin
password admin
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/8ef2a0af-ab08-4be8-809e-6b0b632ed809)
Update New password, This is Sonar Dashboard.

**Step 4A:** Create a **.gitlab-ci.yml** File.
Now go to GitLab click on '+' and click on Newfile
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/706f0430-21a5-43f0-b505-1efbc6eacd88)
File name .gitlab-ci.yml
Content

```sh
stages:
    - npm

Install dependecy:
    stage: npm    
    image:
        name: node:16
    script:
        - npm install
```

![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/935b4e73-c6cb-4843-97a9-384e37b7b801)

Commit the changes and it will automatically start the build
- Now click on Build and Pipelines

![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/7abcd6a3-c85b-47e4-91e2-be8c3591acaa)

- Now click on Running.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/3a53782c-d46c-4bdd-9ce1-fdf23bf20b9f)
- Click on Install dependency
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/b9aa838f-06ff-49c8-a280-b4f978ba5502)
- You will build output
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/2aa56159-a48e-4bf2-9668-a62b1a743707)
- Now add the Sonarqube stage to the pipeline
- Go to the Sonarqube dashboard and click on Manually.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/67bec914-b0df-435d-bc82-c74f49f0a640)
- Provide the name of the Project and click on Setup.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/d1018987-dc72-490e-910a-74042006f780)
- Select the CI tool as GitLab CI.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/5047f9b7-9308-4874-a5a2-df4f68453761)
- Select Other because we are using the JS App
- It will provide code and we need to create a file inside our repo
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/1535558c-d0b3-435f-8797-ba2f5421be24)
- Go to Gitlab and click on + and Newfile.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/8b776643-36c2-4278-a974-74f7d436e12b)
- Filename is sonar-project.properties
- Paste the content that you got from Sonarqube
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/a5779443-4166-4b90-a0cd-696f6feff31c)
- The file looks like this and click on commit changes
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/4596ac06-d137-4dfd-809a-f8d400e78d65)
- Go to Sonarqube and click on continue.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/ef458eb7-5cd7-4dee-a635-ab60b1280fd9)
- Now it will provide Variables to add to our GitLab.
# Step 4B: Add the required variables for the project.
**Variables Generated**
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/86b38191-018d-4f6f-ad1f-81aa41e5275f)
- Now go to GitLab
- Click on settings and CI/CD
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/6ef2ccab-46c9-4a85-a6fb-66f38befd2f5)
- Click on Expand in variables
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/96c6f1ab-944c-4048-a94d-a3119bb5c155)
- Click on Add variable
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/b76feecb-801f-416a-a706-2eb145b633a5)
- Now go back to Sonarqube and copy the Key
- Click on Generate a token
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/8088b52d-1f08-40e6-bca9-4aaface3f399)
- Again Click on Generate
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/0acea737-5ca8-4078-a48d-c45dc41a4495)
- Copy the token.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/9deec478-3bab-4d7f-93f7-098848f6cee4)
- Now come back to GitLab and add them like the below image and click on add variable.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/06c6fcf1-ebb5-4ab3-ba76-471971684e55)
- Sonar token is added
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/6199cb6d-6c0d-404b-a5fd-d7a5bad02b20)
- Now go to the Sonarqube Dashboard again
- Let's add another variable, copy them
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/82bd3319-211f-4527-9059-8171777b7e78)
- Now go to GitLab and click on Add variable
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/0eff5e5c-a96c-48dd-846f-f0f373751af0)
- Add the copied values like the below image
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/ab016db2-2750-4bdd-8d6e-80d7a27c5af7)
- Two variables were added.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/4bcbb17e-1ca7-45e8-b786-88e9c1cec857)

- Now go back to the Sonarqube Dashboard
- Click on continue
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/674c86ca-e98b-4276-9247-db7469a5ca55)
- It will provide and CI configuration file copy it and use it inside our **.gitlab-ci.yml file**
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/8a5763b1-de78-4503-b7e3-0c82f64a786d)
- Now go back to GitLab and edit the .gitlab-ci.yml file
- Full file (update with your content)

```sh
stages:
    - npm
    - sonar

Install dependecy:
    stage: npm    
    image:
        name: node:16
    script:
        - npm install    

sonarqube-check:
  stage: sonar
  image: 
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - sonar-scanner
  allow_failure: true
  only:
    - main
```

![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/44f4eb45-b8ad-431e-8c49-5ea9c5ab6da7)

- Commit changes and it will automatically start the build.
- Click on Build --> Pipelines
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/360e6bf8-3711-47a3-9359-f41fa16f6f8d)
- Click on Running
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/884e0755-0232-44b7-b359-efe7df36208a)
- Now click on Sonarqube-check
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/7576136f-639c-4511-83dc-928c15376511)
- Build output
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/3fb4d755-38b2-473b-a38e-3bcb6774f8ae)

- Now add the next stage of the **Trivy file scan**
- Update the **.gitlab-ci.yml** file

```sh
stages:
    - npm
    - sonar
    - trivy file scan

Install dependecy:
    stage: npm    
    image:
        name: node:16
    script:
        - npm install    

sonarqube-check:
  stage: sonar
  image: 
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - sonar-scanner
  allow_failure: true
  only:
    - main

Trivy file scan:
  stage: trivy file scan
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  script:
    - trivy fs .
```
    
   ![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/7639ae06-4962-4254-b762-b4d1afe39a71)

- Commit changes and go to pipeline stages
- Click on the **Trivy file scan**
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/b1e59e9c-0a4e-43e6-ad2a-f466391a7456)

- Build output
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/9fd8bfbc-b84e-4297-8f94-d6a2a16b6fd5)

- Add the Docker build and push stage
- Before that Add docker credentials to GitLab Variables as secrets.
- Go to the docker hub and create a Personal Access token
- Click on your profile name and Account Settings
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/cb442faa-a10f-4ae8-adb3-aceb7a604a70)
- Now click on Security --> New Access Token
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/53948d33-3efd-44ef-bc8a-7447353b81d2)

- Provide a name --> Generate
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/25c82c92-6dbf-4a90-ad7d-56b1ec6ee281)

- Now copy the token and keep it safe
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/b66a359e-90ac-4475-a788-bfc73696fe11)

- Now go back to Gitlab
- Click on settings and CI/CD
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/1ed29703-d51c-4b89-b0be-e86a90424828)
- Click on Expand in variables
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/45bd01f5-4349-409c-a983-d8d44fe102d3)
- Click on Add variable
- Use your DockerHub username in value and Add variable
- Key DOCKER_USERNAME
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/0ab45914-bb7a-4188-92de-c57f197e94da)
- Click on Add variable again
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/9bbc83e3-014f-4d8b-9639-d096dffcad07)

- Key DOCKER_PASSWORD
- For value use the Generated Personal Access token and add a variable.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/2f15be46-d579-426e-aaa3-be714a9f2354)
- Variables added.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/455eb575-6e9d-4f7b-a00b-4becbdc26c04)
- Now add the below stage to the Configuration .gitlab-ci.yml file
- Added Docker and Trivy image scan stages

```sh
stages:
    - npm
    - sonar
    - trivy file scan
    - docker
    - trivy image scan

Install dependecy:
    stage: npm    
    image:
        name: node:16
    script:
        - npm install    

sonarqube-check:
  stage: sonar
  image: 
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - sonar-scanner
  allow_failure: true
  only:
    - main

Trivy file scan:
  stage: trivy file scan
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  script:
    - trivy fs . 

Docker build and push:
  stage: docker
  image:
    name: docker:latest
  services:
    - docker:dind   
  script:
    - docker build --build-arg REACT_APP_RAPID_API_KEY=f0ead79813mshb0aa7ddf114a7dap1adb3djsn483b017de1a9 -t youtubev1 .    
    - docker tag youtubev1 sevenajay/youtubev1:latest
    - docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
    - docker push sevenajay/youtubev1:latest

Scan image:
  stage: trivy image scan
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  script:
    - trivy image sanjovarghese/youtubev1:latest
```
- Added stages
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/fb048833-469b-4bf1-869f-f7b7e7dbc170)
- Commit changes and it will automatically start building.
- Go to Pipelines view
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/887aadc6-c7ed-4d99-8330-b24864f24c6f)
- Now click on Docker build and push
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/72d369fa-b017-41fc-b4ec-3e7cdb245ba9)
- Build view
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/0487b2ba-9c1e-43b4-8398-7592a6d8bffd)
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/91eaad63-4082-4638-9f26-560f69273e44)
**Go to Dockerhub and see the image**

![ce](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/9d2be298-8cae-4766-b4f6-646574bb1e22)
- Now come back to GitLab and click on Trivy image scan
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/0ed89e56-4b23-411a-a0c7-797c3cea5322)
- Output raw
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/08669142-7353-442a-b81b-0c4995b32172)
# Step 5: Install Gitlab Runner on Ec2
- Go to GitLab and Click on Settings and CI/CD
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/8f1f20ae-4540-471f-b74a-2cc0a112906c)
- Click on Expand at Runners.
  ![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/95d686c7-43d3-4f0a-bfce-9fcbc3ddf1ba)
  - Click on Three dots and then click on Show Runner installation.
 ![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/204a4325-febe-4f11-afcf-3236c382311c)
- Click on Linux and amd64 and copy the commands
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/099584a9-71d8-44f6-a81b-d897e9ac73a8)
- Now come back to Putty or Mobaxtreme
- Create a new file
```sh
sudo vi gitlab-runner-installation
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/77e249be-482c-4cd9-ad00-f44bd8b86da4)
- Paste the below commands into it
# Download the binary for your system
```sh
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
```

# Give it permission to execute
```sh
sudo chmod +x /usr/local/bin/gitlab-runner
```
# Create a GitLab Runner user
```sh
sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
```
# Install and run as a service
```sh
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/e5bb7915-4181-4a2c-a8bc-0004023da73a)
- Provide executable permissions and run the script
```sh
sudo chmod +x <file-name>
./<file-name>
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/a1ee8cd2-530c-48cf-aef9-706cc94e3b2e)
- Installation completed
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/708fc89a-065e-4980-b44a-41f3b7e76d44)
- Start the GitLab runner
```sh
sudo gitlab-runner start
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/fdebaa78-9210-4a45-a653-0b9490e206ab)
- Now run the below command or your command to register the runner
- Update the token is enough
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/1670b5cf-550e-4690-94c8-4594efde0144)
```sh
sudo gitlab-runner register --url https://gitlab.com/ --registration-token <token>
```
**Provide the details for registering the runner**
- 1.	Provide Enter for GitLab.com
- 2.	For token we already added with token, so click on Enter again
- 3.	Description as your wish
- 4.	Tags also and you can use multiple tags by providing a comma after each tga
- 5.	Maintenance note is just optional
- 6.	For executors use Shell
 ![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/439f8624-c287-44d1-80f4-ab4f87656e0b)

- Runner added successfully.
- Start the GitLab runner

```sh
sudo gitlab-runner start
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/227e6296-3f73-48ea-8dfd-c97d5652f9b8)

- Run the GitLab runner
```sh
sudo gitlab-runner run
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/50d1e59c-f4ba-4991-819c-f830f1701347)
- Go to GitLab and refresh the page once or click on Enable for this project
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/b38e8a70-14fa-4f0a-8b9d-e7b9f7aa3e43)
- Now the runner is active and waiting for jobs
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/a703f58d-db0a-41f2-a71b-73da2a664b04)
- Click on the Pencil mark to edit
  ![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/5f9f9131-744f-4c06-a2be-b864ae1ebb31)
- Click on the Check box to indicate whether this runner can pick jobs without tags.
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/e3fffb3a-5e57-4dbe-b76d-c219c80af70f)
- Click on save changes.
# Step 6: Run the Application on the Docker container
- Now edit the **.gitlab-ci.yml**file for the deploy stage
- The complete file

 ```sh
  stages:
    - npm
    - sonar
    - trivy file scan
    - docker
    - trivy image scan
    - run container
Install dependecy:
    stage: npm    
    image:
        name: node:16
    script:
        - npm install    
sonarqube-check:
  stage: sonar
  image: 
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - sonar-scanner
  allow_failure: true
  only:
    - main
Trivy file scan:
  stage: trivy file scan
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  script:
    - trivy fs . 
Docker build and push:
  stage: docker
  image:
    name: docker:latest
  services:
    - docker:dind   
  script:
    - docker build --build-arg REACT_APP_RAPID_API_KEY=f0ead79813mshb0aa7ddf114a7dap1adb3djsn483b017de1a9 -t youtubev1 .    
    - docker tag youtubev1 sanjovarghese/youtubev1:latest
    - docker login -u $DOCKER_USERNAME -p $DOCKER_PASSWORD
    - docker push sevenajay/youtubev1:latest
Scan image:
  stage: trivy image scan
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  script:
    - trivy image sanjovarghese/youtubev1:latest
deploy:
  stage: run container
  tags:
    - youtube        #use your own tags 
  script:
    - docker run -d --name youtube -p 3000:3000 sanjovarghese/youtubev1:latest
```


- Commit changes, it will automatically start to build
- Click on Build --> Pipelines
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/653978bb-29ce-4b89-803f-b52a6082f9a3)
- Click on Running
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/b35f5f27-0463-41c0-b2f0-0e44b57ecdca)
- The last stage is added to the Pipeline
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/f2832835-caf7-4243-bede-3f5f190a459a)
- If you get an error like this
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/e6a8727c-5b8b-4689-80a5-da389a4f545f)
- Go to GitLab and click on deploy job
- Let's see what is the error
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/350449af-7b35-461f-a75c-b11953bb8317)
- If you get an error like this, click on that link
![cerrr](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/bbff8ab5-8947-40cc-a65a-b118e99c7db2)
- It will open a new tab and provide a solution for that
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/9f4a4b3e-df45-4832-8c72-f745e40b2528)

- Now go to Mobaxtreme and stop the Runner
- Go to root and use the below commands
```sh
sudo su
sudo vi /home/gitlab-runner/.bash_logout
```
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/c2705f8d-bb76-4565-8419-9631895f9b93)

- You will see file like this
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/39ac68f4-827c-4000-aafb-6e579c5c7af6)
- Comment them
 ![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/b2fa5b04-a913-49e3-8192-4feef01c837b)
- Save and exit from that file and restart GitLab runner
```sh
sudo gitlab-runner restart
```
exit #from root
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/f1043cf5-b0ce-482f-bf7c-69efccb21a9b)

- Now start and Run the GitLab runner
```sh
sudo gitlab-runner start
sudo gitlab-runner run
```

![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/4262dbd5-3f53-46d2-8436-bce60294e703)
- Now go to GitLab --> Build --> Pipelines
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/b32966b8-25ca-4abc-b5e3-df6982cc6a2b)
- Click on Run Pipeline
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/ebcd61ac-d10d-40f2-811f-4cd5cc215681)
- Again Click on Run Pipeline
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/0e31fdb5-6d30-447f-9ed2-e2f3e3a1a18e)
- Build completed and click on Deploy job
![image](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/d6fe3061-19ff-4e89-b77f-e3d1203c3937)
- See the output it ran a container on ec2
![vgy](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/e8d19257-2e9b-4c62-9fd6-1c3397df256a)
- Now go to MobaXtreme or Putty and Provide the below command to see running containers.
  
```sh
docker ps
```
- Container running.

# Step 7: Access the Application on Browser
- Copy the Public IP of the ec2 instance and paste it into the Browser.
- Don't forget to open the 3000 port in the Security Group
```sh
<Public-ip:3000>
```
![ttyyy](https://github.com/Sanjo-varghese/DevSecOps-Youtube/assets/116708794/f58b8f53-d8b4-47e5-bb2b-3578b3850d1c)

# Step 8: Termination
- 1.	Delete the personal Access token of the docker.
- 2.	Delete the containers.
  ```sh
   docker stop <container name>
   docker rm <container name>
  ```
  # Delete the Ec2 instance.

# Thanks 
























